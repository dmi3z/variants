export class Genre {
  id: number;
  name: string;
  name_en: string;
  is_main: boolean;
  ch_count?: number;
}

export class Category {
  id: number;
  name: string;
  name_en: string;
  genres: Genre[];
}

export interface MenuItem   {
  id: number;
  name: string;
  image: string;
  subcategories?: MenuItem[];
  path: string;
}
