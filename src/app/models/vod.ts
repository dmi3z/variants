export interface Tvshow {
  tvshow_id: string;
  channel_id: number;
  title: string;
  date: string;
  start: number;
  stop: number;
  video_id: number;
  deleted: number;
  ts: number;
  cover?: string;
}

export interface Video {
  age_rating: string;
  art: string;
  cast: any[];
  category_id: number;
  countries: string[];
  cover: string;
  description: string;
  director: number[];
  duration: number;
  episodes: Episode[];
  genres: number[] | string[];
  international_name: string;
  is_series: boolean;
  name: string;
  ratings: Raitings;
  video_id: number;
  year: string;
  in_products?: InProduct[];
  is_pladform: boolean;
  pladform_id?: number;
  tvshow_id?: string;
}

interface InProduct {
  product_id: number;
  product_options: ProductOption[];
}

interface ProductOption {
  option_id: number;
  cost: string;
  currency: string;
  term: string;
}

export interface Country {
  id: number;
  name: string;
}

export interface RatingFilter {
  id: number;
  name: string;
  value: string;
}

export interface Raiting {
  count: number;
  system_uid: string;
  value: number;
}

export interface Raitings {
  imdb: Raiting;
  kinopoisk: Raiting;
  local: Raiting;
}
export interface Episode {
    episode: string;
    season: string;
    type: string;
    video_id: number;
    tvshow_id?: string;
}

export type ContentType = 'tv' | 'video' | 'channel' | 'audiobook' | 'audiobook-trial';

// tslint:disable-next-line: no-namespace
export namespace ContentName {
    export const TV = 'tv';
    export const VIDEO = 'video';
    export const CHANNEL = 'channel';
    export const BOOK = 'audiobook';
}
