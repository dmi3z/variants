import { CommonModule } from '@angular/common';
import { ContentCardComponent } from './content-card.component';
import { NgModule } from '@angular/core';
import { PipesModule } from 'src/app/pipes/pipes.module';


@NgModule({
  declarations: [
    ContentCardComponent
  ],
  imports: [
    CommonModule,
    PipesModule
  ],
  exports: [
    ContentCardComponent
  ]
})

export class ContentCardModule { }
