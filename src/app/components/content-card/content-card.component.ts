import { Component, Input, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Tvshow, Video,  ContentType, ContentName } from 'src/app/models/vod';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content-card',
  templateUrl: './content-card.component.html',
  styleUrls: ['./content-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ContentCardComponent implements OnInit {

  @Input() data: Tvshow | Video;
  @Input() cardDesign: ContentType;
  @Input() countInRow = 5;

  public video: Video;
  public tvshow: Tvshow;
  public genres: string[] = [];

  public isBrowser: boolean;
  public showElement: boolean;

  public isCourse: boolean;

  private type: ContentType;

  constructor(
    private dataService: DataService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.data.tvshow_id) {
      this.video = this.data as Video;
      this.type = ContentName.VIDEO;
    } else {
      this.tvshow = this.data as Tvshow;
      this.type = ContentName.TV;
      this.dataService.getVideosInfo([this.tvshow.video_id]).then(result => {
        this.video = result[0];
        this.cdr.markForCheck();
      });
    }
  }

  public get cardWidth(): number {
    return Math.round(100 / this.countInRow) - 1;
  }

  public get isTvDesign(): boolean {
    return this.cardDesign === ContentName.TV;
  }

  public showDescription(): void {
    /* if (this.isCourse) {
      const ids = [
        1047632, // Photoshop
        966238,  // Android
        1606986, // Phyton,
        1041757  // English
      ];
      if (ids.includes(+this.video.video_id)) {
        this.router.navigate(['/course-page', this.video.video_id]);
      } else {
        this.router.navigate(['/details', this.contentName]);
      }
    } else {
      this.router.navigate(['/details', this.contentName]);
    } */

    const id = this.type === ContentName.TV ? this.tvshow.tvshow_id : this.video.video_id;

    this.router.navigate(['description', this.type, id], { state: 
      {
        video: this.video,
        tvshow: this.tvshow
      }
    });

  }
}
