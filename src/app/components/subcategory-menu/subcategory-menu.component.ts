import { Component, Input } from "@angular/core";
import { MenuItem } from '../../models/category';

@Component({
    selector: 'app-subcategory-menu',
    templateUrl: 'subcategory-menu.component.html',
    styleUrls: ['subcategory-menu.component.scss']
})

export class SubcategoryMenuComponent {
    @Input() categories: MenuItem[];

    constructor() { }
}
