import { NgModule } from "@angular/core";
import { SubcategoryMenuComponent } from './subcategory-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        SubcategoryMenuComponent
    ],
    imports: [
        CommonModule,
        RouterModule
    ],
    exports: [
        SubcategoryMenuComponent
    ]
})

export class SubcategoryMenuModule { }
