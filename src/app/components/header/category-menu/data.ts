import { MenuItem } from 'src/app/models/category';

export const menu: MenuItem[] = [
  /* {
    id: 0,
    name: '1',
    image: 'watch.jpg',
    path: '/watch'
  },
  {
    id: 1,
    name: '2',
    image: 'listen.jpg',
    path: '/listen'
  },
  {
    id: 2,
    name: '3',
    image: 'read.jpg',
    path: '/read'
  },
  {
    id: 3,
    name: '4',
    image: 'learn.jpg',
    path: '/learn'
  },
  {
    id: 4,
    name: '5',
    image: 'learn.jpg',
    path: '/supernew'
  },
  {
    id: 5,
    name: '6',
    image: 'learn.jpg',
    path: '/supersupernew'
  },
  {
    id: 6,
    name: '7',
    image: 'learn.jpg',
    path: '/all-genres'
  }, */
  {
    id: 7,
    name: '12',
    image: 'learn.jpg',
    path: 'ch1'
  },
  {
    id: 8,
    name: '12-2',
    image: 'learn.jpg',
    path: 'ch2'
  },
  {
    id: 9,
    name: '13',
    image: 'learn.jpg',
    path: 'ch13'
  },
  {
    id: 10,
    name: '13-1',
    image: 'learn.jpg',
    path: 'ch13-1'
  },
  {
    id: 11,
    name: '13-2',
    image: 'learn.jpg',
    path: 'ch13-2'
  },
  {
    id: 12,
    name: '13-3',
    image: 'learn.jpg',
    path: 'ch13-3'
  },
  {
    id: 13,
    name: '14-1',
    image: 'learn.jpg',
    path: 'ch14'
  },
  {
    id: 14,
    name: '14-2',
    image: 'learn.jpg',
    path: 'ch14-2'
  },
  {
    id: 14,
    name: 'f3',
    image: 'learn.jpg',
    path: 'f3'
  },
  {
    id: 15,
    name: 'f4',
    image: 'learn.jpg',
    path: 'f4'
  },
  {
    id: 16,
    name: 'f5',
    image: 'learn.jpg',
    path: 'f5'
  },
  {
    id: 17,
    name: 'f6',
    image: 'learn.jpg',
    path: 'f6'
  },
  {
    id: 18,
    name: 'f7',
    image: 'learn.jpg',
    path: 'f7'
  }
];

