import { Component, OnInit } from '@angular/core';
import { menu } from './data';
import { MenuItem } from 'src/app/models/category';

@Component({
  selector: 'app-category-menu',
  templateUrl: 'category-menu.component.html',
  styleUrls: ['category-menu.component.scss']
})

export class CategoryMenuComponent implements OnInit {

  public menuItems: MenuItem[] = menu;
  public selectedCategory: MenuItem;

  constructor() { }

  ngOnInit() {
    this.selectedCategory = this.menuItems[0];
  }

  public changeCategory(category: MenuItem): void {
    if (category.id !== this.selectedCategory.id) {
      this.selectedCategory = category;
    }
  }

  public isCategorySelected(categoryId: number): boolean {
    return this.selectedCategory.id === categoryId;
  }

}
