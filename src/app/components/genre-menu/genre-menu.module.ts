import { NgModule } from '@angular/core';
import { GenreMenuComponent } from './genre-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IsMainPipe } from './ismain.pipe';

@NgModule({
  declarations: [
    GenreMenuComponent,
    IsMainPipe
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    GenreMenuComponent
  ]
})


export class GenreMenuModule { }
