import { Pipe, PipeTransform } from '@angular/core';
import { Genre } from 'src/app/models/category';

@Pipe({
  name: 'isMainPipe'
})


export class IsMainPipe implements PipeTransform {
  transform(genres: Genre[]): Genre[] {
    return genres.filter(genre => genre.is_main);
  }
}
