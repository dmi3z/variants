import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Genre } from 'src/app/models/category';
import { Params, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-genre-menu',
  templateUrl: 'genre-menu.component.html',
  styleUrls: ['genre-menu.component.scss']
})

export class GenreMenuComponent {

  @Input() genres: Genre[];
  @Input() activeGenreId: number;
  @Output() genreChange = new EventEmitter<number>();

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  public onGenreChange(genre: Genre): void {
    const queryParams: Params = { id: genre.id };

    this.router.navigate([],
      {
        relativeTo: this.activatedRoute,
        queryParams,
        queryParamsHandling: 'merge',
      });
    this.genreChange.emit(genre.id);
  }

  public isGenreActive(genreId): boolean {
    return this.activeGenreId === genreId;
  }

}
