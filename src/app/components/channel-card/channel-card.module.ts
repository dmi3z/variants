import { CommonModule } from '@angular/common';
import { ChannelCardComponent } from './channel-card.component';
import { NgModule } from '@angular/core';
import { TitlePipe } from './title.pipe';
import { ProgressPipe } from './progress.pipe';
import { DeferLoadModule } from '@trademe/ng-defer-load';

@NgModule({
  declarations: [
    ChannelCardComponent,
    TitlePipe,
    ProgressPipe
  ],
  imports: [
    CommonModule,
    DeferLoadModule
  ],
  exports: [
    ChannelCardComponent
  ]
})

export class ChannelCardModule {}
