import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy, Inject, PLATFORM_ID } from '@angular/core';
import { Channel } from 'src/app/models/channel';
import { Tvshow } from 'src/app/models/vod';
import { DataState } from 'src/app/redux/data/data.state';
import { AssistantService } from 'src/app/services/assistant.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-channel-card',
  templateUrl: './channel-card.component.html',
  styleUrls: ['./channel-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelCardComponent implements OnInit {
  @Input() channel: Channel;
  @Output() showAllEvent = new EventEmitter<any>();
  @Input() currentTime: number;
  public cover: string;
  public isShow: boolean;
  public isBrowser: boolean;
  public tvshow: Observable<Tvshow>;

  constructor(
    // private imageService: ImageService,
    private dataStore: Store<DataState>,
    private assistantService: AssistantService,
    @Inject(PLATFORM_ID) private platform: any
  ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadTvshow();
  }

  private loadTvshow(): void {
    this.tvshow = this.dataStore.select('storeData').pipe(map(data => {
      const tv = data.currentTvshows.find(tvshow => +tvshow.channel_id === this.channel.channel_id);
      if (tv) {
        return tv;
      }
      const stubTvshow: Tvshow = {
        channel_id: 10,
        start: 0,
        stop: 0,
        title: 'Нет данных',
        date: null,
        deleted: 0,
        ts: 0,
        tvshow_id: null,
        video_id: 0
      };
      return stubTvshow;
    }));
  }

  public playChannel(): void {
    // TODO: redirect to player
  }

  public showItem() {
    this.isShow = true;
    this.cover = this.assistantService.getChannelFrame(this.channel.channel_id, this.currentTime);
  }

}
