import { Component, Input } from '@angular/core';
import { AudioBook } from 'src/app/models/litres.model';
import { AudiobooksService } from 'src/app/services/audiobooks.service';
import { Video, ContentName } from 'src/app/models/vod';
import { Router } from '@angular/router';

@Component({
  selector: 'app-audiobook-card',
  templateUrl: 'audiobook-card.component.html',
  styleUrls: ['audiobook-card.component.scss']
})

export class AudiobookCardComponent {

  @Input() book: AudioBook;

  constructor(
    private audiobookService: AudiobooksService,
    private router: Router
  ) { }

  public get cover(): string {
    return this.audiobookService.getCover(this.book.id, this.book.cover);
  }

  public openDescription(): void {
    const book = this.book;
    const data: Video = {
      name: book.title,
      international_name: null,
      age_rating: book.adult.toString(),
      countries: [],
      art: book.contract_title,
      cast: book.authors ? book.authors.map(author => author["first-name"] + ' ' + author["last-name"]) : [],
      category_id: 8,
      cover: this.cover,
      description: book.annotation,
      director: [],
      duration: book.chars,
      year: book.date_written_s ? book.date_written_s.toString() : null,
      episodes: [],
      genres: book.genres.map(genre => genre.title),
      is_pladform: false,
      is_series: false,
      ratings: null,
      video_id: book.id
    };
    this.router.navigate(['description', ContentName.BOOK, book.id], { state: 
      {
        video: data
      }
    });
  }
}
