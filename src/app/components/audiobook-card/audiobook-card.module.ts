import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AudiobookCardComponent } from './audiobook-card.component';

@NgModule({
  declarations: [
    AudiobookCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AudiobookCardComponent
  ]
})

export class AudiobookCardModule { }
