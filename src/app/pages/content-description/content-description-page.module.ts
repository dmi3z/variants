import { NgModule } from "@angular/core";
import { ContentDescriptionPageComponent } from './content-description-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CastsPipe } from './cast.pipe';
import { StartTimePipe } from './start-time.pipe';
import { DurationTimePipe } from './duration-time.pipe';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
    declarations: [
        ContentDescriptionPageComponent,
        CastsPipe,
        StartTimePipe,
        DurationTimePipe
    ],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: ContentDescriptionPageComponent
            }
        ])
    ]
})

export class ContentDescriptionPageModule { }
