import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Tvshow, Video, ContentName, ContentType, Episode } from 'src/app/models/vod';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { map } from 'rxjs/operators';
import { Person, Persons } from 'src/app/models/person';
import { DataService } from 'src/app/services/data.service';
import { AudiobooksService } from 'src/app/services/audiobooks.service';
import { AudioBook } from 'src/app/models/litres.model';

@Component({
    selector: 'app-content-description-page',
    templateUrl: 'content-description-page.component.html',
    styleUrls: ['content-description-page.component.scss']
})

export class ContentDescriptionPageComponent implements OnInit {

    // Main data
    public video: Video;
    public tvshow: Tvshow;

    // Loaded data
    public channel: Observable<Channel>;
    public casts: Observable<Person[]> | string[];
    public directors: Observable<Person[]>;
    public bookInfo: AudioBook;

    private content: ContentData;
    private type: ContentType;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private dataStore: Store<DataState>,
        private dataService: DataService,
        private audiobooksService: AudiobooksService
    ) {
        this.content = this.router.getCurrentNavigation().extras.state as ContentData;
    }

    ngOnInit() {
        this.type = this.activatedRoute.snapshot.params.type as ContentType;
        if (this.content) {
            this.video = this.content.video;
            this.tvshow = this.content.tvshow;
            this.loadChannelInfo();
            this.loadCasts();
            this.loadDirectors();
        } else {
            const id = this.activatedRoute.snapshot.params.id;
            switch (this.type) {
                case ContentName.VIDEO:
                    this.loadVideoInfo(id);
                    break;
                case ContentName.TV:
                    this.loadTvshowInfo(id);
                    break;
                case ContentName.BOOK:
                    this.loadBookInfo(id);
                    break;
                default:
                    break;
            }
        }
    }

    public get isAudiobook(): boolean {
        return this.type === ContentName.BOOK;
    }

    private loadChannelInfo(): void {
        if (this.tvshow && this.tvshow.channel_id) {
            this.channel = this.dataStore.select('storeData')
                .pipe(map(data => data.channels))
                .pipe(map(res => res.find(channel => channel.channel_id === Number(this.tvshow.channel_id))));
        }
    }

    private loadCasts(): void {
        if (this.video && this.video.cast && this.video.cast.length > 0) {
            if (this.isAudiobook) {
                this.casts = this.video.cast;
            } else {
                const ids = this.video.cast.map(item => Number(item));
                this.casts = this.dataService.loadVodPersons(ids);
            }
        }
    }

    private loadDirectors(): void {
        if (this.video && this.video.director && this.video.director.length > 0) {
            this.directors = this.dataService.loadVodPersons(this.video.director);
        }
    }

    // --- NO DATA STATE -----

    private loadVideoInfo(id: number): void {
        this.dataService.getVideosInfo([id]).then(res => {
            this.video = res[0];
            this.loadCasts();
            this.loadDirectors();
        });
    }

    private loadTvshowInfo(id: string): void {
        this.dataService.getTvshowInfo(id).then(res => {
            this.tvshow = res;
            this.loadChannelInfo();
            this.loadVideoInfo(res.video_id);
        });
    }

    private loadBookInfo(id: number): void {
        this.audiobooksService.getBooksInfo([id]).then(res => {
            const book = res[0];
            if (this.video) {
                this.video.is_series = book.file_groups[1].files && book.file_groups[1].files.length > 1 ? true : false
            } else {
                this.video = {
                    description: book.annotation,
                    name: book.title,
                    age_rating: book.adult ? book.adult.toString() : null,
                    category_id: 7,
                    duration: book.chars,
                    genres: book.genres.map(genre => genre.title),
                    episodes: null,
                    is_pladform: false,
                    is_series: book.file_groups[1].files && book.file_groups[1].files.length > 1 ? true : false,
                    international_name: null,
                    year: book.date_written_s ? book.date_written_s.toString() : null,
                    ratings: null,
                    tvshow_id: null,
                    video_id: null,
                    cover: this.audiobooksService.getCover(id, book.cover),
                    countries: null,
                    cast: book.authors ? book.authors.map(author => author["first-name"] + ' ' + author["last-name"]) : [],
                    art: book.contract_title,
                    director: []
                };
            }
            this.bookInfo = book;
        })
    }
}

interface ContentData {
    video: Video;
    tvshow: Tvshow;
}
