import { NgModule } from "@angular/core";
import { CoursesPageComponent } from './courses-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ContentCardModule } from 'src/app/components/content-card/content-card.module';

@NgModule({
    declarations: [
        CoursesPageComponent
    ],
    imports: [
        CommonModule,
        GenreMenuModule,
        InfiniteScrollModule,
        ContentCardModule,
        RouterModule.forChild([
            {
                path: '',
                component: CoursesPageComponent
            }
        ])
    ]
})

export class CoursesPageModule { }
