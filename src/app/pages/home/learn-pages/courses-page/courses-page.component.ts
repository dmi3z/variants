import { Component, OnInit } from "@angular/core";
import { Genre } from 'src/app/models/category';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Video, Tvshow } from 'src/app/models/vod';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-courses-page',
    templateUrl: 'courses-page.component.html',
    styleUrls: ['courses-page.component.scss']
})

export class CoursesPageComponent implements OnInit {

    public genres: Observable<Genre[]>;
    public activeGenreId: number;
    private categoryId = 6; // Courses

    private size = 20;
    private offset = 0;

    public content: Video[] | Tvshow[] = [];

    constructor(
        private dataStore: Store<DataState>,
        private activatedRoute: ActivatedRoute,
        private dataService: DataService
    ) { }

    ngOnInit() {
        this.activeGenreId = Number(this.activatedRoute.snapshot.queryParams.id);
        if (!this.activeGenreId) {
            this.activeGenreId = 0;
        }
        this.genres = this.dataStore.select('storeData').pipe(map(data => {
            const category = data.vodCategories.find(cat => cat.id === this.categoryId);
            if (category) {
                const genres = category.genres;
                return genres;
            }
            return [];
        }));

        this.loadContent();
    }

    public loadContent(): void {
        this.dataService.getContent(this.categoryId, this.activeGenreId, this.size, this.offset).then(result => {
            this.content.push(...result.videos);
            this.offset += this.size;
        });
    }

    public onGenreChange(genreId: number): void {
        if (this.activeGenreId !== genreId) {
            this.activeGenreId = genreId;
            this.offset = 0;
            this.content = [];
            this.loadContent();
        }
    }

}
