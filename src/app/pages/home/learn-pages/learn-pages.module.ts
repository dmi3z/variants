import { NgModule } from "@angular/core";
import { LearnPagesComponent } from './learn-pages.component';
import { CommonModule } from '@angular/common';
import { SubcategoryMenuModule } from 'src/app/components/subcategory-menu/subcategory-menu.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        LearnPagesComponent
    ],
    imports: [
        CommonModule,
        SubcategoryMenuModule,
        RouterModule.forChild([
            {
                path: '',
                component: LearnPagesComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'courses'
                    },
                    {
                        path: 'courses',
                        loadChildren: () => import('./courses-page/courses-page.module').then(m => m.CoursesPageModule)
                    }
                ]
            }
        ])
    ]
})

export class LearnPagesModule { }
