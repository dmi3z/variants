import { NgModule } from "@angular/core";
import { F5PageComponent } from './f5-page.component';
import { MainMenuModule } from '../f4/main-menu/main-menu.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        F5PageComponent
    ],
    imports: [
        MainMenuModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: F5PageComponent
            }
        ])
    ]
})

export class F5PageModule { }
