import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';
import { Genre } from 'src/app/models/category';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-films-5-page',
    templateUrl: 'f5-page.component.html',
    styleUrls: ['f5-page.component.scss']
})

export class F5PageComponent implements OnInit {
    
      public selectedChapter: MenuItem;
      public selectedCategory: MenuElem;
      public selectedGenre: Genre;
      public isChaptersOpened: boolean;
      public isCategoryOpened: boolean;
      public isGenresOpened: boolean;

    public content = [];
    public genres: Observable<Genre[]>;

    private clickedCardId: any;

    constructor(private dataService: DataService, private dataStore: Store<DataState>, ) { }

    ngOnInit() {
        this.dataService.getContent(1, 881, 20, 0).then(res => this.content = res.videos);
      }
    
    
      public changeChapter(item: MenuItem): void {
        this.selectedChapter = item;
        this.selectedCategory = item.elems[0];
        this.toggleChapters();
      }
    
      public changeCategory(item: MenuElem): void {
        this.selectedCategory = item;
        this.toggleCategory();
      }
    
      public changeGenre(genre: Genre): void {
        this.selectedGenre = genre;
        this.toggleGenres();
      }
    
      public toggleChapters(): void {
        this.isChaptersOpened = !this.isChaptersOpened;
      }
    
      public toggleCategory(): void {
        this.isCategoryOpened = !this.isCategoryOpened;
      }
    
      public toggleGenres(): void {
        this.isGenresOpened = !this.isGenresOpened;
      }

      public isClicked(id: any): boolean {
          return this.clickedCardId === id;
      }

      public selectCard(id: any): void {
        this.clickedCardId = id;
      }

}

interface MenuItem {
    id: number;
    name: string;
    elems: MenuElem[];
  }
  
  interface MenuElem {
    id: number;
    name: string;
    image: string;
  }
