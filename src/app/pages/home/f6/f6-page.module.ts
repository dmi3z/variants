import { NgModule } from "@angular/core";
import { F6PageComponent } from './f6-page.component';
import { MainMenuModule } from '../f4/main-menu/main-menu.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        F6PageComponent
    ],
    imports: [
        MainMenuModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: F6PageComponent
            }
        ])
    ]
})

export class F6PageModule { }
