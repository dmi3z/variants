import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { AssistantService } from 'src/app/services/assistant.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-ch2-page',
  templateUrl: 'ch2-page.component.html',
  styleUrls: ['ch2-page.component.scss']
})

export class Ch2PageComponent implements OnInit {
  public posterChannel: Observable<Channel>;
  public channels: Observable<Channel[]>;

  constructor(private dataStore: Store<DataState>, private assistantService: AssistantService) { }

  ngOnInit() {
    this.channels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681))));
    this.posterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[55]));
  }

  public get currentTime(): number {
    return this.assistantService.currentTime;
  }

  public getCover(id: number, width?: number, height?: number): string {
    return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
  }
}
