import { NgModule } from '@angular/core';
import { HomePageComponent } from './home-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SupernewPageComponent } from './supernew-page/supernew-page.component';
import { SuperSupernewPageComponent } from './supersupernew-page/supersupernew-page.component';
import { AllGenresPageComponent } from './all-genres/all-genres-page.component';
import { Films1PageComponent } from './films-1-page/films-1-page.component';
import { Ch1PageComponent } from './ch1/ch1-page.component';
import { Ch2PageComponent } from './ch2/ch2-page.component';
import { F2PageComponent } from './f2/f2-page.component';
import { Ch13PageComponent } from './ch13/ch13-page.component';
import { Ch131PageComponent } from './ch13-1/ch13-1-page.component';
import { Ch132PageComponent } from './ch13-2/ch13-2-page.component';
import { Ch133PageComponent } from './ch13-3/ch13-3-page.component';
import { Ch14PageComponent } from './ch14/ch14-page.component';
import { Ch142PageComponent } from './ch14-2/ch14-2-page.component';
import { F3PageComponent } from './f3/f3-page.component';

@NgModule({
  declarations: [
    HomePageComponent,
    SupernewPageComponent,
    SuperSupernewPageComponent,
    AllGenresPageComponent,
    Films1PageComponent,
    Ch1PageComponent,
    Ch2PageComponent,
    F2PageComponent,
    Ch13PageComponent,
    Ch131PageComponent,
    Ch132PageComponent,
    Ch133PageComponent,
    Ch14PageComponent,
    Ch142PageComponent,
    F3PageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePageComponent,
        children: [
          {
            path: '',
            redirectTo: 'watch',
            pathMatch: 'full'
          },
          {
            path: 'watch',
            loadChildren: () => import('./watch-pages/watch-page.module').then(m => m.WatchPageModule)
          },
          {
            path: 'listen',
            loadChildren: () => import('./listen-pages/listen-page.module').then(m => m.ListenPageModule)
          },
          {
            path: 'learn',
            loadChildren: () => import('./learn-pages/learn-pages.module').then(m => m.LearnPagesModule)
          },
          {
            path: 'read',
            loadChildren: () => import('./read-pages/read-pages.module').then(m => m.ReadPagesModule)
          },
          {
            path: 'supernew',
            component: SupernewPageComponent
          },
          {
            path: 'supersupernew',
            component: SuperSupernewPageComponent
          },
          {
            path: 'all-genres',
            component: AllGenresPageComponent
          },
          {
            path: 'f1',
            component: Films1PageComponent
          },
          {
            path: 'f2',
            component: F2PageComponent
          },
          {
            path: 'f3',
            component: F3PageComponent
          },
          {
            path: 'ch1',
            component: Ch1PageComponent
          },
          {
            path: 'ch2',
            component: Ch2PageComponent
          },
          {
            path: 'ch13',
            component: Ch13PageComponent
          },
          {
            path: 'ch13-1',
            component: Ch131PageComponent
          },
          {
            path: 'ch13-2',
            component: Ch132PageComponent
          },
          {
            path: 'ch13-3',
            component: Ch133PageComponent
          },
          {
            path: 'ch14',
            component: Ch14PageComponent
          },
          {
            path: 'ch14-2',
            component: Ch142PageComponent
          },
          {
            path: 'f4',
            loadChildren: () => import('./f4/f4-page.module').then(m => m.F4PageModule)
          },
          {
            path: 'f5',
            loadChildren: () => import('./f5/f5-page.module').then(m => m.F5PageModule)
          },
          {
            path: 'f6',
            loadChildren: () => import('./f6/f6-page.module').then(m => m.F6PageModule)
          },
          {
            path: 'f7',
            loadChildren: () => import('./f7/f7-page.module').then(m => m.F7PageModule)
          }
        ]
      }
    ])
  ]
})

export class HomePageModule { }
