import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-films-1-page',
    templateUrl: 'films-1-page.component.html',
    styleUrls: ['films-1-page.component.scss']
})

export class Films1PageComponent implements OnInit {

    public content = [];

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getBanners().then(res => {
            const player = <HTMLVideoElement>document.getElementById('video');
            player.src = res[0].trailer_url_desktop;
            player.volume = 0;
            player.muted = true;
            player.play();
        });

        this.dataService.getContent(1, 881, 20, 0).then(res => this.content = res.videos);
    }

}
