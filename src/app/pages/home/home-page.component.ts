import { Component } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: 'home-page.component.html',
  styleUrls: ['home-page.component.scss']
})


export class HomePageComponent {

  public categoryId: number;

  constructor() { }

  onCategoryChange(categoryId: number): void {
    this.categoryId = categoryId;
  }
}
