import { Component } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-f2-page',
    templateUrl: 'f2-page.component.html',
    styleUrls: ['f2-page.component.scss']
})

export class F2PageComponent {
    public content = [];

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getBanners().then(res => {
            const player = <HTMLVideoElement>document.getElementById('video');
            player.src = res[0].trailer_url_desktop;
            player.volume = 0;
            player.muted = true;
            player.play();
        });

        this.dataService.getContent(1, 881, 20, 0).then(res => this.content = res.videos);
    }

    public getNameWithColor(name: string): string {
        if (name) {
          const nameArray: string[] = name.split(' ');
          if (nameArray.length === 1) {
            return name;
          }
          let colorString: string = nameArray.shift();
          colorString += ' <span class="orange-text">';
          const lastText: string = nameArray.toString();
          colorString += lastText.replace(new RegExp(',', 'g'), ' ');
          colorString += '</span>';
          return colorString;
        }
      }
}
