import { Component } from "@angular/core";
import { NguCarouselStore, NguCarouselConfig } from '@ngu/carousel';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { DataState } from 'src/app/redux/data/data.state';
import { Store } from '@ngrx/store';
import { AssistantService } from 'src/app/services/assistant.service';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';

@Component({
    selector: 'app-read-pages',
    templateUrl: 'read-pages.component.html',
    styleUrls: ['read-pages.component.scss']
})

export class ReadPagesComponent {

  public posterChannel: Observable<Channel>;
  // public nextPosterChannel: Observable<Channel>;
  public channels: Observable<Channel[]>;
  // public hdChannels: Observable<Channel[]>;


  constructor(private dataService: DataService, private dataStore: Store<DataState>, private assistantService: AssistantService) { }

  ngOnInit() {
    this.dataService.getBanners().then(res => {
      const player = <HTMLVideoElement>document.getElementById('video');
      player.src = res[0].trailer_url_desktop;
      player.volume = 0;
      player.muted = true;
      player.play();
    });
    this.channels = this.dataStore.select('storeData').pipe(map(res =>res.channels.filter(ch => ch.genres.includes(681))));
    this.posterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[55]));
    // this.nextPosterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[74]));
    // this.hdChannels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681)).slice(0, 5)));
    /* 
    player.volume = 0;
    player.muted = true;
    player.play(); */
  }

  public get currentTime(): number {
    return this.assistantService.currentTime;
  }

  public getCover(id: number, width?: number, height?: number): string {
    return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
  }


}
