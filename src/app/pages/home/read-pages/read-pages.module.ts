import { NgModule } from "@angular/core";
import { ReadPagesComponent } from './read-pages.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NguCarouselModule } from '@ngu/carousel';

@NgModule({
    declarations: [
        ReadPagesComponent
    ],
    imports: [
        CommonModule,
        NguCarouselModule,
        RouterModule.forChild([
            {
                path: '',
                component: ReadPagesComponent
            }
        ])
    ]
})

export class ReadPagesModule { }
