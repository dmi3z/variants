import { NgModule } from "@angular/core";
import { F7PageComponent } from './f7-page.component';
import { MainMenuModule } from '../f4/main-menu/main-menu.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SlickModule } from 'ngx-slick';

@NgModule({
    declarations: [
        F7PageComponent
    ],
    imports: [
        MainMenuModule,
        CommonModule,
        SlickModule.forRoot(),
        RouterModule.forChild([
            {
                path: '',
                component: F7PageComponent
            }
        ])
    ]
})

export class F7PageModule { }
