import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from 'src/app/services/data.service';
import { Genre, MenuItem } from 'src/app/models/category';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { Observable } from 'rxjs';
import { SlickComponent } from 'ngx-slick';

@Component({
  selector: 'app-films-7-page',
  templateUrl: 'f7-page.component.html',
  styleUrls: ['f7-page.component.scss']
})

export class F7PageComponent implements OnInit {

  public content = [];

  public menuItems: MenuItem[] = [
    {
      id: 0,
      name: 'ТВ',
      path: 'null',
      image: 'menu1.jpg'
    },
    {
      id: 1,
      name: 'Фильмы',
      path: 'null',
      image: 'menu2.jpg'
    },
    {
      id: 2,
      name: 'Сериалы',
      path: 'null',
      image: 'menu3.jpg'
    },
    {
      id: 3,
      name: 'Передачи',
      path: 'null',
      image: 'menu1.jpg'
    },
    {
      id: 4,
      name: 'Мультфильмы',
      path: 'null',
      image: 'menu2.jpg'
    },
  ];

  slideConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    dots: false,
    prevArrow: false,
    nextArrow: false,
    centerMode: true,
    centerPadding: '350px',
    infinite: true
  };

  @ViewChild('slickModal', {static: true }) slickSlider: SlickComponent;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getContent(1, 881, 20, 0).then(res => this.content = res.videos);
  }


  public scrollTo(index): void {
    this.slickSlider.slickGoTo(index);
  }

}


