import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Channel } from 'src/app/models/channel';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { map } from 'rxjs/operators';
import { AssistantService } from 'src/app/services/assistant.service';
import { Genre } from 'src/app/models/category';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-tv-review-page',
    templateUrl: 'tv-review-page.component.html',
    styleUrls: ['tv-review-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class TvReviewPageComponent implements OnInit {

  public channels: Observable<Channel[]>;
  public currentTime: number;
  public genres: Observable<Genre[]>;
  public activeGenreId: number;

  constructor(
    private dataStore: Store<DataState>,
    private assistantService: AssistantService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.currentTime = this.assistantService.currentTime;
    this.genres = this.dataStore.select('storeData').pipe(map(data => data.channelGenres));
    this.activeGenreId = Number(this.activatedRoute.snapshot.queryParams.id);
    if (!this.activeGenreId) {
      this.activeGenreId = 0;
    }
    this.channels = this.dataStore.select('storeData').pipe(map(data => this.getFilteredChannels(data.channels)));
  }

  private getFilteredChannels(channels: Channel[]): Channel[] {
    if (this.activeGenreId === 0) {
      return channels;
    }
    return channels.filter(ch => ch.genres.includes(this.activeGenreId));
  }

  public onCurrentGenreChange(genreId: number): void {
    if (this.activeGenreId !== genreId) {
      this.activeGenreId = genreId;
      this.channels = this.dataStore.select('storeData').pipe(map(data => this.getFilteredChannels(data.channels)));
    }
  }
}
