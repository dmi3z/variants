import { NgModule } from '@angular/core';
import { TvReviewPageComponent } from './tv-review-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';
import { ChannelCardModule } from 'src/app/components/channel-card/channel-card.module';

@NgModule({
    declarations: [
        TvReviewPageComponent
    ],
    imports: [
        CommonModule,
        ChannelCardModule,
        GenreMenuModule,
        RouterModule.forChild([
            {
                path: '',
                component: TvReviewPageComponent
            }
        ])
    ]
})

export class TvReviewPageModule { }
