import { NgModule } from "@angular/core";
import { CartoonsPageComponent } from './cartoons-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ContentCardModule } from 'src/app/components/content-card/content-card.module';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';

@NgModule({
    declarations: [
        CartoonsPageComponent
    ],
    imports: [
        CommonModule,
        InfiniteScrollModule,
        ContentCardModule,
        GenreMenuModule,
        RouterModule.forChild([
            {
                path: '',
                component: CartoonsPageComponent
            }
        ])
    ]
})

export class CartoonsPageModule { }
