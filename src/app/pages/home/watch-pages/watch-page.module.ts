import { NgModule } from "@angular/core";
import { WatchPageComponent } from './watch-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SubcategoryMenuModule } from 'src/app/components/subcategory-menu/subcategory-menu.module';
import { ChannelCardModule } from 'src/app/components/channel-card/channel-card.module';

@NgModule({
    declarations: [
        WatchPageComponent
    ],
    imports: [
        CommonModule,
        SubcategoryMenuModule,
        ChannelCardModule,
        RouterModule.forChild([
            {
                path: '',
                component: WatchPageComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'tv-review'
                    },
                    {
                        path: 'tv-review',
                        loadChildren: () => import('./tv-review/tv-review-page.module').then(m => m.TvReviewPageModule)
                    },
                    {
                        path: 'films',
                        loadChildren: () => import('./films/films-page.module').then(m => m.FilmsPageModule)
                    },
                    {
                        path: 'blogers',
                        loadChildren: () => import('./blogers/blogers-page.module').then(m => m.BlogersPageModule)
                    },
                    {
                        path: 'series',
                        loadChildren: () => import('./series/series-page.module').then(m => m.SeriesPageModule)
                    },
                    {
                        path: 'tvshows',
                        loadChildren: () => import('./tvshows/tvshows-page.module').then(m => m.TvshowsPageModule)
                    },
                    {
                        path: 'cartoons',
                        loadChildren: () => import('./cartoons/cartoons-page.module').then(m => m.CartoonsPageModule)
                    }
                ]
            }
        ])
    ]
})

export class WatchPageModule { }
