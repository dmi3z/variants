import { NgModule } from "@angular/core";
import { BlogersPageComponent } from './blogers-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentCardModule } from 'src/app/components/content-card/content-card.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    declarations: [
        BlogersPageComponent
    ],
    imports: [
        CommonModule,
        ContentCardModule,
        InfiniteScrollModule,
        RouterModule.forChild([
            {
                path: '',
                component: BlogersPageComponent
            }
        ])
    ]
})

export class BlogersPageModule { }
