import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/models/category';
import { DataService } from 'src/app/services/data.service';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { map } from 'rxjs/operators';
import { AssistantService } from 'src/app/services/assistant.service';

@Component({
  selector: 'app-watch-page',
  templateUrl: 'watch-page.component.html',
  styleUrls: ['watch-page.component.scss']
})

export class WatchPageComponent implements OnInit {

  public posterChannel: Observable<Channel>;
  // public nextPosterChannel: Observable<Channel>;
  public channels: Observable<Channel[]>;
  // public hdChannels: Observable<Channel[]>;

  public step = 5;

  public categories: MenuItem[] = [
    {
      id: 4,
      name: 'ТВ',
      image: 'cat.jpg',
      path: 'tv-review'
    },
    {
      id: 5,
      name: 'Кино',
      image: 'cat.jpg',
      path: 'films'
    },
    {
      id: 6,
      name: 'Сериалы',
      image: 'cat.jpg',
      path: 'series'
    },
    {
      id: 7,
      name: 'Передачи',
      image: 'cat.jpg',
      path: 'tvshows'
    },
    {
      id: 8,
      name: 'Блогеры',
      image: 'cat.jpg',
      path: 'blogers'
    },
    {
      id: 9,
      name: 'Мультфильмы',
      image: 'cat.jpg',
      path: 'cartoons'
    },
  ];

  constructor(private dataService: DataService, private dataStore: Store<DataState>, private assistantService: AssistantService) { }

  ngOnInit() {
    /* this.dataService.getBanners().then(res => {
      const player = <HTMLVideoElement>document.getElementById('video');
      player.src = res[0].trailer_url_desktop;
      player.volume = 0;
      player.muted = true;
      player.play();
    }); */
    this.channels = this.dataStore.select('storeData').pipe(map(res =>res.channels.filter(ch => ch.genres.includes(681))));
    this.posterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[55]));
    // this.nextPosterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[74]));
    // this.hdChannels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681)).slice(0, 5)));
    /* 
    player.volume = 0;
    player.muted = true;
    player.play(); */
  }

  public get currentTime(): number {
    return this.assistantService.currentTime;
  }

  public getCover(id: number, width?: number, height?: number): string {
    return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
  }

  public nextStep(): void {
    if (this.step >= 1) {
      this.step--;
      /* if (this.step === 4) {
        this.channels = this.dataStore.select('storeData').pipe(map(res => res.channels.slice(0, this.step + 1)));
      } */
      this.channels = this.dataStore.select('storeData').pipe(map(res => res.channels.slice(0, this.step + 1)));
    }
  }
}
