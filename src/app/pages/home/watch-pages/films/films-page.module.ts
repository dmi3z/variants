import { NgModule } from '@angular/core';
import { FilmsPageComponent } from './films-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';
import { ContentCardModule } from 'src/app/components/content-card/content-card.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    FilmsPageComponent
  ],
  imports: [
    CommonModule,
    GenreMenuModule,
    ContentCardModule,
    InfiniteScrollModule,
    RouterModule.forChild([
      {
        path: '',
        component: FilmsPageComponent
      }
    ])
  ]
})

export class FilmsPageModule { }
