import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs';
import { Genre } from 'src/app/models/category';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-series-page',
    templateUrl: 'series-page.component.html'
})

export class SeriesPageComponent implements OnInit { 
    private readonly categoryId = 2; // series

  public genres: Observable<Genre[]>;
  public activeGenreId: number;
  public content: any[] = [];

  private size = 20;
  private offset = 0;

  constructor(
    private dataStore: Store<DataState>,
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.activeGenreId = Number(this.activatedRoute.snapshot.queryParams.id);
    if (!this.activeGenreId) {
      this.activeGenreId = 0;
    }
    this.genres = this.dataStore.select('storeData').pipe(map(data => {
      const category = data.vodCategories.find(cat => cat.id === this.categoryId);
      if (category) {
        const genres = category.genres;
        return genres;
      }
      return [];
    }));

    this.loadContent();
  }

  public loadContent(): void {
    this.dataService.getContent(this.categoryId, this.activeGenreId, this.size, this.offset).then(result => {
      this.content.push(...result.videos);
      this.offset += this.size;
    });
  }

  public onCurrentGenreChange(genreId: number): void {
    if (this.activeGenreId !== genreId) {
      this.activeGenreId = genreId;
      this.content = [];
      this.offset = 0;
      this.loadContent();
    }
  }
}
