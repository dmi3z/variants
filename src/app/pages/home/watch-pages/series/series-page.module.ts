import { NgModule } from "@angular/core";
import { SeriesPageComponent } from './series-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';
import { ContentCardModule } from 'src/app/components/content-card/content-card.module';

@NgModule({
    declarations: [
        SeriesPageComponent
    ],
    imports: [
        CommonModule,
        InfiniteScrollModule,
        GenreMenuModule,
        ContentCardModule,
        RouterModule.forChild([
            {
                path: '',
                component: SeriesPageComponent
            }
        ])
    ]
})

export class SeriesPageModule { }
