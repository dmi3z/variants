import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { AssistantService } from 'src/app/services/assistant.service';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-ch13-3-page',
  templateUrl: 'ch13-3-page.component.html',
  styleUrls: ['ch13-3-page.component.scss']
})

export class Ch133PageComponent implements OnInit {
  public posterChannel: Observable<Channel>;
  public channels: Observable<Channel[]>;

  public menuItems = [
    {
      name: 'Тв',
      text1: '124 программ передач',
      text2: 'Смотреть ТВ',
      image: 'menu1.jpg'
    },
    {
      name: 'Кино',
      text1: '124 программ передач',
      text2: 'Смотреть Кино',
      image: 'menu2.jpg'
    },
    {
      name: 'Сериалы',
      text1: '124 сериала',
      text2: 'Смотреть Сериалы',
      image: 'menu3.jpg'
    }
  ]

  constructor(private dataService: DataService, private dataStore: Store<DataState>, private assistantService: AssistantService) { }

  ngOnInit() {
    this.dataService.getBanners().then(res => {
      const player = <HTMLVideoElement>document.getElementById('video');
      player.src = res.find(item => +item.banner_id === 114).trailer_url_desktop;
      player.volume = 0;
      player.muted = true;
      player.play();
    });
    this.channels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681))));
    this.posterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[55]));
  }

  public get currentTime(): number {
    return this.assistantService.currentTime;
  }

  public getCover(id: number, width?: number, height?: number): string {
    return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
  }
}
