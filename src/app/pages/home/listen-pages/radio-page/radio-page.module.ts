import { NgModule } from "@angular/core";
import { RadioPageComponent } from './radio-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        RadioPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: RadioPageComponent
            }
        ])
    ]
})

export class RadioPageModule { }
