import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AudiobooksPageComponent } from './audiobooks-page.component';
import { GenreMenuModule } from 'src/app/components/genre-menu/genre-menu.module';
import { AudiobookCardModule } from 'src/app/components/audiobook-card/audiobook-card.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    declarations: [
        AudiobooksPageComponent
    ],
    imports: [
        CommonModule,
        GenreMenuModule,
        AudiobookCardModule,
        InfiniteScrollModule,
        RouterModule.forChild([
            {
                path: '',
                component: AudiobooksPageComponent
            }
        ])
    ]
})

export class AudiobooksPageModule { }
