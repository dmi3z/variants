import { Component, OnInit } from "@angular/core";
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { AudiobooksService } from 'src/app/services/audiobooks.service';
import { Observable } from 'rxjs';
import { Genre } from 'src/app/models/category';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AudioBook } from 'src/app/models/litres.model';

@Component({
    selector: 'app-audiobooks-page',
    templateUrl: 'audiobooks-page.component.html',
    styleUrls: ['audiobooks-page.component.scss']
})

export class AudiobooksPageComponent implements OnInit {

    public genres: Observable<Genre[]>;
    public activeGenreId: number;

    public books: AudioBook[] = [];

    private size = 20;
    private offset = 0;

    constructor(
        private dataStore: Store<DataState>,
        private audiobookService: AudiobooksService,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.activeGenreId = Number(this.activatedRoute.snapshot.queryParams.id);
        if (!this.activeGenreId) {
          this.activeGenreId = 0;
        }
        this.audiobookService.loadGenres();
        this.genres = this.dataStore.select('storeData').pipe(map(data => data.audiobookGenres));
        this.loadContent();
    }

    public onGenreChange(genreId: number): void {
        if (this.activeGenreId !== genreId) {
            this.activeGenreId = genreId;
            this.offset = 0;
            this.books = [];
            this.loadContent();
        }
    }

    public loadContent(): void {
        this.audiobookService.getList(this.activeGenreId, this.offset, this.size).then(data => this.books.push(...data));
        this.offset += this.size;
    }
}
