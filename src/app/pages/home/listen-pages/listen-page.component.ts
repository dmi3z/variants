import { Component } from "@angular/core";
import { MenuItem } from 'src/app/models/category';
import { DataService } from 'src/app/services/data.service';
import { AssistantService } from 'src/app/services/assistant.service';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-listen-page',
    templateUrl: 'listen-page.component.html',
    styleUrls: ['listen-page.component.scss']
})

export class ListenPageComponent {

    public channels: Observable<Channel[]>;
    

    constructor(private dataService: DataService, private assistantService: AssistantService, private dataStore: Store<DataState>) { }

    ngOnInit() {
        this.dataService.getBanners().then(res => {
          const player = <HTMLVideoElement>document.getElementById('video');
          player.src = res[0].trailer_url_desktop;
          player.volume = 0;
          player.muted = true;
          player.play();
        });
        this.channels = this.dataStore.select('storeData').pipe(map(res =>res.channels.filter(ch => ch.genres.includes(681))));
        // this.nextPosterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[74]));
        // this.hdChannels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681)).slice(0, 5)));
        /* 
        player.volume = 0;
        player.muted = true;
        player.play(); */
      }
    
      public get currentTime(): number {
        return this.assistantService.currentTime;
      }
    
      public getCover(id: number, width?: number, height?: number): string {
        return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
      }
    
}
