import { NgModule } from "@angular/core";
import { ListenPageComponent } from './listen-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SubcategoryMenuModule } from 'src/app/components/subcategory-menu/subcategory-menu.module';

@NgModule({
    declarations: [
        ListenPageComponent
    ],
    imports: [
        CommonModule,
        SubcategoryMenuModule,
        RouterModule.forChild([
            {
                path: '',
                component: ListenPageComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'audiobooks'
                    },
                    {
                        path: 'radio',
                        loadChildren: () => import('./radio-page/radio-page.module').then(m => m.RadioPageModule)
                    },
                    {
                        path: 'audiobooks',
                        loadChildren: () => import('./audiobooks-page/audiobooks-page.module').then(m => m.AudiobooksPageModule)
                    }
                ]
            }
        ])
    ]
})

export class ListenPageModule { }
