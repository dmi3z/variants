import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';
import { Genre } from 'src/app/models/category';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-films-3-page',
    templateUrl: 'f3-page.component.html',
    styleUrls: ['f3-page.component.scss']
})

export class F3PageComponent implements OnInit {
    public menuItems: MenuItem[] = [
        {
          id: 0,
          name: 'Смотри',
          elems: [
            {
              id: 0,
              name: 'ТВ',
              image: 'live.png'
            },
            {
              id: 1,
              name: 'Фильмы',
              image: 'film.png'
            },
            {
              id: 2,
              name: 'Сериалы',
              image: 'serial.png'
            },
            {
              id: 3,
              name: 'Передачи',
              image: 'pere.png'
            },
            {
              id: 4,
              name: 'Блогеры',
              image: 'live.png'
            },
            {
              id: 5,
              name: 'Мультфильмы',
              image: 'mult.png'
            }
          ]
        },
        {
          id: 1,
          name: 'Слушай',
          elems: [
            {
              id: 0,
              name: 'Аудиокниги',
              image: 'audio.png'
            },
            {
              id: 1,
              name: 'Радио',
              image: 'live.png'
            }
          ]
        },
        {
          id: 2,
          name: 'Обучайся',
          elems: [
            {
              id: 0,
              name: 'Курсы',
              image: 'courses.png'
            }
          ]
        },
        {
          id: 3,
          name: 'Читай',
          elems: [
            {
              id: 0,
              name: 'Книги',
              image: 'courses.png'
            }
          ]
        }
      ];
    
      public selectedChapter: MenuItem;
      public selectedCategory: MenuElem;
      public selectedGenre: Genre;
      public isChaptersOpened: boolean;
      public isCategoryOpened: boolean;
      public isGenresOpened: boolean;

    public content = [];
    public genres: Observable<Genre[]>;

    private clickedCardId: any;

    constructor(private dataService: DataService, private dataStore: Store<DataState>, ) { }

    ngOnInit() {
        this.dataService.getContent(1, 881, 20, 0).then(res => this.content = res.videos);
        this.selectedChapter = this.menuItems[0];
        this.selectedCategory = this.selectedChapter.elems[0];
        this.genres = this.dataStore.select('storeData').pipe(map(res => {
          this.selectedGenre = res.channelGenres[0];
          return res.channelGenres;
        }));
      }
    
    
      public changeChapter(item: MenuItem): void {
        this.selectedChapter = item;
        this.selectedCategory = item.elems[0];
        this.toggleChapters();
      }
    
      public changeCategory(item: MenuElem): void {
        this.selectedCategory = item;
        this.toggleCategory();
      }
    
      public changeGenre(genre: Genre): void {
        this.selectedGenre = genre;
        this.toggleGenres();
      }
    
      public toggleChapters(): void {
        this.isChaptersOpened = !this.isChaptersOpened;
      }
    
      public toggleCategory(): void {
        this.isCategoryOpened = !this.isCategoryOpened;
      }
    
      public toggleGenres(): void {
        this.isGenresOpened = !this.isGenresOpened;
      }

      public isClicked(id: any): boolean {
          return this.clickedCardId === id;
      }

      public selectCard(id: any): void {
        this.clickedCardId = id;
      }

}

interface MenuItem {
    id: number;
    name: string;
    elems: MenuElem[];
  }
  
  interface MenuElem {
    id: number;
    name: string;
    image: string;
  }
