import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/models/channel';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';
import { AssistantService } from 'src/app/services/assistant.service';
import { map } from 'rxjs/operators';
import { Genre } from 'src/app/models/category';

@Component({
  selector: 'app-ch14-2-page',
  templateUrl: 'ch14-2-page.component.html',
  styleUrls: ['ch14-2-page.component.scss']
})

export class Ch142PageComponent implements OnInit {
  public posterChannel: Observable<Channel>;
  public channels: Observable<Channel[]>;
  public genres: Observable<Genre[]>;

  public menuItems: MenuItem[] = [
    {
      id: 0,
      name: 'Смотри',
      elems: [
        {
          id: 0,
          name: 'ТВ',
          image: 'live.png'
        },
        {
          id: 1,
          name: 'Фильмы',
          image: 'film.png'
        },
        {
          id: 2,
          name: 'Сериалы',
          image: 'serial.png'
        },
        {
          id: 3,
          name: 'Передачи',
          image: 'pere.png'
        },
        {
          id: 4,
          name: 'Блогеры',
          image: 'live.png'
        },
        {
          id: 5,
          name: 'Мультфильмы',
          image: 'mult.png'
        }
      ]
    },
    {
      id: 1,
      name: 'Слушай',
      elems: [
        {
          id: 0,
          name: 'Аудиокниги',
          image: 'audio.png'
        },
        {
          id: 1,
          name: 'Радио',
          image: 'live.png'
        }
      ]
    },
    {
      id: 2,
      name: 'Обучайся',
      elems: [
        {
          id: 0,
          name: 'Курсы',
          image: 'courses.png'
        }
      ]
    },
    {
      id: 3,
      name: 'Читай',
      elems: [
        {
          id: 0,
          name: 'Книги',
          image: 'courses.png'
        }
      ]
    }
  ];

  public selectedChapter: MenuItem;
  public selectedCategory: MenuElem;
  public selectedGenre: Genre;
  public isChaptersOpened: boolean;
  public isCategoryOpened: boolean;
  public isGenresOpened: boolean;

  constructor(private dataStore: Store<DataState>, private assistantService: AssistantService) { }

  ngOnInit() {
    this.selectedChapter = this.menuItems[0];
    this.selectedCategory = this.selectedChapter.elems[0];
    this.channels = this.dataStore.select('storeData').pipe(map(res => res.channels.filter(ch => ch.genres.includes(681))));
    this.genres = this.dataStore.select('storeData').pipe(map(res => {
      this.selectedGenre = res.channelGenres[0];
      return res.channelGenres;
    }));
    this.posterChannel = this.dataStore.select('storeData').pipe(map(res => res.channels[55]));
  }

  public get currentTime(): number {
    return this.assistantService.currentTime;
  }

  public getCover(id: number, width?: number, height?: number): string {
    return this.assistantService.getChannelFrame(id, this.currentTime, 'crop', width, height);
  }

  public changeChapter(item: MenuItem): void {
    this.selectedChapter = item;
    this.selectedCategory = item.elems[0];
    this.toggleChapters();
  }

  public changeCategory(item: MenuElem): void {
    this.selectedCategory = item;
    this.toggleCategory();
  }

  public changeGenre(genre: Genre): void {
    this.selectedGenre = genre;
    this.toggleGenres();
  }

  public toggleChapters(): void {
    this.isChaptersOpened = !this.isChaptersOpened;
  }

  public toggleCategory(): void {
    this.isCategoryOpened = !this.isCategoryOpened;
  }

  public toggleGenres(): void {
    this.isGenresOpened = !this.isGenresOpened;
  }
}

interface MenuItem {
  id: number;
  name: string;
  elems: MenuElem[];
}

interface MenuElem {
  id: number;
  name: string;
  image: string;
}
