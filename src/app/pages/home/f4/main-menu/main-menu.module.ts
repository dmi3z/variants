import { NgModule } from "@angular/core";
import { MainMenuComponent } from './main-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        MainMenuComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([])
    ],
    exports: [
        MainMenuComponent
    ]
})

export class MainMenuModule { }
