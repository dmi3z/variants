import { Component } from "@angular/core";

@Component({
  selector: 'app-main-menu',
  templateUrl: 'main-menu.component.html',
  styleUrls: ['main-menu.component.scss']
})

export class MainMenuComponent {

  public menuItems: MenuItem[] = [
    {
      id: 0,
      name: 'Смотри',
      elems: [
        {
          name: 'ТВ',
          image: 'live.png',
          path: 'chs'

        },
        {
          name: 'Фильмы',
          image: 'film.png',
          path: 'films'
        },
        {
          name: 'Сериалы',
          image: 'serial.png',
          path: 'series'
        },
        {
          name: 'Передачи',
          image: 'pere.png',
          path: 'tvshows'
        },
        {
          name: 'Блогеры',
          image: 'blogers.png',
          path: 'blogers'
        },
        {
          name: 'Мультфильмы',
          image: 'mult.png',
          path: 'cartoons'
        }
      ]
    },
    {
      id: 1,
      name: 'Слушай',
      elems: [
        {
          name: 'Аудиокниги',
          image: 'audio.png',
          path: 'audiobooks'
        },
        /* {
          name: 'Радио',
          image: 'radio.png'
        } */
      ]
    },
    {
      id: 2,
      name: 'Обучайся',
      elems: [
        {
          name: 'Курсы',
          image: 'courses.png',
          path: 'courses'
        }
      ]
    },
    /* {
      id: 3,
      name: 'Читай',
      elems: [
        {
          name: 'Книги',
          image: 'book.png'
        }
      ]
    } */
  ];

  private selectedMenuId: number;

  constructor() { }

  public isItemSelected(id: number): boolean {
    return this.selectedMenuId === id;
  }

  public openChapter(id: number): void {
    this.selectedMenuId = id;
  }
}


interface MenuItem {
  id: number;
  name: string;
  elems: MenuElem[];
}

interface MenuElem {
  name: string;
  image: string;
  path?: string;
}
