import { NgModule } from "@angular/core";
import { F4PageComponent } from './f4-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChPageComponent } from './ch/ch-page.component';
import { FilmsPageComponent } from './films/films-page.component';
import { SeriesPageComponent } from './series/series-page.component';
import { TvshowsPageComponent } from './tvshows/tvshows-page.component';
import { BlogersPageComponent } from './blogers/blogers-page.component';
import { CartoonsPageComponent } from './cartoons/cartoons-page.component';
import { CoursesPageComponent } from './courses/courses-page.component';
import { AudiobooksPageComponent } from './audiobooks/audiobooks-page.component';
import { MainMenuModule } from './main-menu/main-menu.module';

@NgModule({
    declarations: [
        F4PageComponent,
        ChPageComponent,
        FilmsPageComponent,
        SeriesPageComponent,
        TvshowsPageComponent,
        BlogersPageComponent,
        CartoonsPageComponent,
        CoursesPageComponent,
        AudiobooksPageComponent
    ],
    imports: [
        CommonModule,
        MainMenuModule,
        RouterModule.forChild([
            {
                path: '',
                component: F4PageComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'chs',
                        pathMatch: 'full'
                    },
                    {
                        path: 'chs',
                        component: ChPageComponent
                    },
                    {
                        path: 'films',
                        component: FilmsPageComponent
                    },
                    {
                        path: 'series',
                        component: SeriesPageComponent
                    },
                    {
                        path: 'tvshows',
                        component: TvshowsPageComponent
                    },
                    {
                        path: 'blogers',
                        component: BlogersPageComponent
                    },
                    {
                        path: 'cartoons',
                        component: CartoonsPageComponent
                    },
                    {
                        path: 'courses',
                        component: CoursesPageComponent
                    },
                    {
                        path: 'audiobooks',
                        component: AudiobooksPageComponent
                    }
                ]
            }
        ])
    ]
})

export class F4PageModule { }
