import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-courses-page',
    templateUrl: 'courses-page.component.html',
    styleUrls: ['courses-page.component.scss']
})

export class CoursesPageComponent implements OnInit {

    public content = [];
    
    private clickedCardId: number;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getContent(6, 0, 20, 0).then(res => this.content = res.videos);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }


}
