import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';

@Component({
    selector: 'app-films-page',
    templateUrl: 'films-page.component.html',
    styleUrls: ['films-page.component.scss']
})

export class FilmsPageComponent implements OnInit {

    public content = [];
    
    private clickedCardId: number;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getContent(1, 1058, 20, 0).then(res => this.content = res.videos);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }


}
