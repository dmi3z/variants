import { Component, OnInit } from "@angular/core";
import { AudioBook } from 'src/app/models/litres.model';
import { AudiobooksService } from 'src/app/services/audiobooks.service';

@Component({
    selector: 'app-audiobooks-page',
    templateUrl: 'audiobooks-page.component.html',
    styleUrls: ['audiobooks-page.component.scss']
})

export class AudiobooksPageComponent implements OnInit {

    public books: AudioBook[] = [];
    private clickedCardId: number;

    constructor(
        private audiobookService: AudiobooksService,
    ) { }

    ngOnInit() {
        this.loadContent();
    }


    public loadContent(): void {
        this.audiobookService.getList(0, 0, 20).then(data => this.books = data);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }

    public getCover(id, cover): string {
        return this.audiobookService.getCover(id, cover);
    }
}
