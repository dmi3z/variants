import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-blogers-page',
    templateUrl: 'blogers-page.component.html',
    styleUrls: ['blogers-page.component.scss']
})

export class BlogersPageComponent implements OnInit {

    public content = [];
    
    private clickedCardId: number;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getContent(4, 892, 20, 0).then(res => this.content = res.videos);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }


}
