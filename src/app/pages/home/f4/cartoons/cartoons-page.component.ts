import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-cartoons-page',
    templateUrl: 'cartoons-page.component.html',
    styleUrls: ['cartoons-page.component.scss']
})

export class CartoonsPageComponent implements OnInit {

    public content = [];
    
    private clickedCardId: number;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getContent(3, 890, 20, 0).then(res => this.content = res.videos);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }


}
