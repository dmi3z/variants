import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-series-page',
    templateUrl: 'series-page.component.html',
    styleUrls: ['series-page.component.scss']
})

export class SeriesPageComponent implements OnInit {

    public content = [];
    
    private clickedCardId: number;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dataService.getContent(2, 0, 20, 0).then(res => this.content = res.videos);
    }

    public isClicked(id: any): boolean {
        return this.clickedCardId === id;
    }

    public selectCard(id: any): void {
        this.clickedCardId = id;
    }


}
