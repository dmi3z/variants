import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })

export class AssistantService {

  get currentTime(): number {
    return moment().unix();
  }

  getChannelFrame(id: number, time: number, transform: string = 'crop', width: number = 500, height: number = 300): string {
    const t = Math.round(time);
    return `https://old.persik.by/utils/show-frame.php?c=${id}&t=${t}&tr=${transform}&w=${width}&h=${height}`;
  }
}
