import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ParamInterceptor implements HttpInterceptor {

  constructor() { }

  // get device(): string {
  //   const envDevice = environment.platform;
  //   switch (envDevice) {
  //     case 'android':
  //       return 'android-by';
  //     case 'samsung':
  //       return 'samsung-by';
  //     case 'lg':
  //       return 'lg-by';
  //     default:
  //       return 'browser-by';
  //   }
  // }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const paramReq = req.clone({
      params: req.params.set('device', 'web-by') //.set('auth_token', this.authService.token).set('uuid', this.authService.uuid)
    });
    return next.handle(paramReq);
  }
}
