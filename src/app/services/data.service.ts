import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Genre, Category } from '../models/category';
import { DataState } from '../redux/data/data.state';
import { Store } from '@ngrx/store';
import { LoadChannelGenres, LoadVodCategories, LoadChannels, LoadCurrentTvshows } from '../redux/data/data.action';
import { Tvshow, Video, Country, RatingFilter } from '../models/vod';
import { Channel } from '../models/channel';
import { map } from 'rxjs/operators';
import { Persons, Person } from '../models/person';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class DataService {

  private BASE_URL = 'https://api.persik.by/';

  constructor(private http: HttpClient, private dataStore: Store<DataState>) { }

  loadChannelCategories(): void {
    this.http.get(this.BASE_URL.concat('v2/categories/channel'))
      .toPromise().then((categories: Genre[]) => {
        if (categories[0].id !== 0) {
          categories.unshift({
            id: 0,
            name: 'Все жанры',
            name_en: 'All genres',
            is_main: true
          });
        }
        this.dataStore.dispatch(new LoadChannelGenres(categories));
      });
  }

  loadVodCategories(): void {
    this.http.get(this.BASE_URL.concat('v2/categories/video'))
      .toPromise().then((categories: Category[]) => {
        categories.forEach(category => {
          if (category.genres[0].id !== 0) {
            category.genres.unshift({
              id: 0,
              name: 'Все жанры',
              name_en: 'All genres',
              is_main: true
            });
          }
        });
        this.dataStore.dispatch(new LoadVodCategories(categories));
      });
  }

  loadChannels(): void {
    this.http.get<{ channels: Channel[] }>(this.BASE_URL.concat('v2/content/channels')).toPromise().then(data => {
      data.channels.sort((a, b) => {
        return b.rank - a.rank;
      });
      const mainChannels = data.channels;
      this.dataStore.dispatch(new LoadChannels(mainChannels));
      const ids = mainChannels.map(channel => channel.channel_id);
      this.getCurrentTvShows(ids).then(tvshows => {
        this.dataStore.dispatch(new LoadCurrentTvshows(tvshows));
      });
    });
  }

  getCurrentTvShows(channelIds: number[]): Promise<Tvshow[]> {
    let queryString = '?';
    channelIds.forEach((item, index) => {
      if (index !== channelIds.length - 1) {
        queryString += `channels[]=${item}&`;
      } else {
        queryString += `channels[]=${item}`;
      }
    });
    return this.http.get<any>(this.BASE_URL.concat('v2/epg/onair', queryString)).pipe(map(response => response.tvshows))
      .toPromise();
  }

  getVideosInfo(ids: any[]): Promise<Video[]> {
    let queryString = '?';
    ids.forEach((item, index) => {
      if (index !== ids.length - 1) {
        queryString += `id[]=${item}&`;
      } else {
        queryString += `id[]=${item}`;
      }
    });
    return this.http.get<{ videos: Video[] }>(this.BASE_URL.concat('v2/content/video', queryString)).pipe(map(res => {
      if (res && res.videos) {
        return res.videos;
      }
      return null;
    })).toPromise();
  }

  getContent(categoryId: number, genreId: number, size?: number,
    skip?: number, country?: Country, sort?: RatingFilter): Promise<any> {

    const URL = 'http://api.persik.by/v2/content/videos';
    let type = 'sort';
    if (sort && sort.value.split(' ').length === 2) {
      type = 'year';
    }
    const params: HttpParams = new HttpParams()
      .set('size', (size && size !== 0) ? size.toString() : '')
      .set('offset', (skip && skip !== 0) ? skip.toString() : '')
      .set('category_id', categoryId.toString())
      .set('genre_id', (genreId !== -1 && genreId) ? genreId.toString() : '')
      .set('country_id', (country && country.id !== 0) ? country.id.toString() : '')
      .set('sort', (type === 'sort' && sort && sort.value) ? sort.value : 'last')
      .set('year_from', (type === 'year') ? sort.value.split(' ')[0] : '')
      .set('year_to', (type === 'year') ? sort.value.split(' ')[1] : '')
      .set('detail', '1');
    return this.http.get<any>(URL, { params }).toPromise();
  }


  loadVodPersons(ids: number[]): Observable<Person[]> {
    let paramString = '';
    ids.forEach(id => {
      paramString += `id[]=${id}&`;
    });
    paramString.slice(-1);
    return this.http.get<Persons>(this.BASE_URL.concat('v2/content/person?', paramString)).pipe(map(response => {
      return response.persons;
    }));
  }

  getTvshowInfo(id: string): Promise<Tvshow> {
    const params: HttpParams = new HttpParams()
      .set('id[]', id);
    return this.http.get<{ tvshows: Tvshow[] }>(this.BASE_URL.concat('v2/content/tvshow'), { params })
      .pipe(map(res => res.tvshows[0])).toPromise();
  }

  getBanners(): Promise<any[]> {
    return this.http.get<any[]>(this.BASE_URL.concat('v2/content/banners2')).pipe(map(res => {
      return res.filter(banner => banner.trailer_url_desktop.length > 0);
    })).toPromise();
  }


}
