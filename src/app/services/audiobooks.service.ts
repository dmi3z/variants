import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { AudioBookGenre, AudioBook } from '../models/litres.model';
import { Store } from '@ngrx/store';
import { DataState } from '../redux/data/data.state';
import { map, take } from 'rxjs/operators';
import { LoadAudiobookGenres } from '../redux/data/data.action';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class AudiobooksService {

    private readonly LITRES_BASE_URL = 'https://api.persik.by/v2/litres';

    constructor(private http: HttpClient, private dataStore: Store<DataState>) { }

    private get isHaveGenres(): boolean {
        let genres = [];
        this.dataStore.select('storeData').pipe(map(data => data.audiobookGenres)).pipe(take(1)).subscribe(res => genres = res);
        return genres.length > 0;
    }

    loadGenres(): void {
        if (!this.isHaveGenres) {
            this.http.get<AudioBookGenre[]>(this.LITRES_BASE_URL.concat('/genres')).toPromise()
                .then(data => {
                    data.unshift({
                        count: 100000,
                        litres_genre_id: 0,
                        name: 'Бестселлеры',
                        type: null,
                        token: null
                    });
                    const genres = data.map(genre => {
                        return {
                            id: genre.litres_genre_id,
                            is_main: true,
                            name: genre.name,
                            name_en: genre.token
                        }
                    });
                    this.dataStore.dispatch(new LoadAudiobookGenres(genres));
                });
        }
    }

    getCover(id: number, coverFormat = 'jpg'): string {
        return 'https://partnersdnld.litres.ru/pub/c/cover/' + id + '.' + coverFormat;
    }

    getList(genreId: number, offset: number, size: number): Promise<AudioBook[]> {
        const paramsData = {
            size,
            offset
        };
        if (genreId !== -1) {
            Object.assign(paramsData, { genre_id: genreId });
        }
        let params = new HttpParams();
        Object.keys(paramsData).forEach(key => {
            params = params.set(key, paramsData[key]);
        });
        return this.http.get<AudioBook[]>(this.LITRES_BASE_URL, { params }).toPromise();
    }

    getBooksInfo(ids: number[]): Promise<AudioBook[]> {
        let paramString = '';
        ids.forEach(id => {
          paramString += `id[]=${id}&`;
        });
        paramString.slice(-1);
        return this.http.get<AudioBook[]>(this.LITRES_BASE_URL.concat('/item?', paramString)).toPromise();
      }
    
      getUserBooks(): Observable<AudioBook[]> {
        const params = new HttpParams()
          .set('bought', '1');
        return this.http.get<AudioBook[]>(this.LITRES_BASE_URL, { params });
      }
}
