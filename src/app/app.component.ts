import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public isHide: boolean;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.dataService.loadChannelCategories();
    this.dataService.loadVodCategories();
    this.dataService.loadChannels();

    this.router.events.subscribe(_ => {
      if (this.router.url.includes('ch14') || 
      this.router.url.includes('f3') || 
      this.router.url.includes('f4')  || 
      this.router.url.includes('f5') || 
      this.router.url.includes('f6') || 
      this.router.url.includes('f7')
      ) {
        this.isHide = true;
      } else {
        this.isHide = false;
      }
    });
  }
}
