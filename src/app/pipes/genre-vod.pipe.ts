import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngrx/store';
import { DataState } from 'src/app/redux/data/data.state';

@Pipe({
  name: 'vodGenrePipe'
})

export class VodGenrePipe implements PipeTransform {

  constructor(private dataStore: Store<DataState>) { }

  transform(genreIds: number[]): Observable<any> {
    if (genreIds && genreIds.length > 0) {
      return this.dataStore.select('storeData')
        .pipe(map(data => data.vodCategories.map(cat => cat.genres)))
        .pipe(map(data => data.reduce((accum, genres) => accum.concat(genres), [])))
        .pipe(map(data => data.filter(genre => genreIds.includes(genre.id))))
        .pipe(map(result => result.map(genre => genre.name.replace(/^[0-9]+[)]/, ''))));
    }
  }
}
