import { NgModule } from "@angular/core";
import { VodGenrePipe } from './genre-vod.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        VodGenrePipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        VodGenrePipe
    ]
})

export class PipesModule { }
