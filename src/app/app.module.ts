import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { DataReducer } from './redux/data/data.reducer';
import { ParamInterceptor } from './services/api.interceptor';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HeaderModule } from './components/header/header.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    TransferHttpCacheModule,
    HttpClientModule,
    HeaderModule,
    StoreModule.forRoot(
      {
        storeData: DataReducer,
      }),
    BrowserModule.withServerTransition({ appId: 'serverApp' })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
