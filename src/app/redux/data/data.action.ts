// import { Tvshow } from './../../app/models/tvshow';
import { Action } from '@ngrx/store';
import { Genre, Category } from '../../models/category';
import { Channel } from '../../models/channel';
import { Tvshow } from 'src/app/models/vod';

// tslint:disable-next-line: no-namespace
export namespace STORE_ACTIONS {
  export const LOAD_CHANNELS = 'LOAD_CHANNELS';
  export const LOAD_CHANNEL_GENRES = 'LOAD_CHANNEL_GENRES';
  export const LOAD_VOD_CATEGORIES = 'LOAD_VOD_CATEGORIES';
  export const LOAD_AUDIOBOOK_GENRES = 'LOAD_AUDIOBOOK_GENRES';
  export const LOAD_CURRENT_TVSHOWS = 'LOAD_CURRENT_TVSHOWS';
}

export class LoadChannels implements Action {
  readonly type = STORE_ACTIONS.LOAD_CHANNELS;
  constructor(public channels: Channel[]) { }
}

export class LoadChannelGenres implements Action {
  readonly type = STORE_ACTIONS.LOAD_CHANNEL_GENRES;
  constructor(public genres: Genre[]) { }
}

export class LoadAudiobookGenres implements Action {
  readonly type = STORE_ACTIONS.LOAD_AUDIOBOOK_GENRES;
  constructor(public genres: Genre[]) { }
}

export class LoadVodCategories implements Action {
  readonly type = STORE_ACTIONS.LOAD_VOD_CATEGORIES;
  constructor(public categories: Category[]) { }
}

export class LoadCurrentTvshows implements Action {
  readonly type = STORE_ACTIONS.LOAD_CURRENT_TVSHOWS;
  constructor(public tvshows: Tvshow[]) { }
}


export type DataActions = LoadChannels | LoadChannelGenres | LoadVodCategories | LoadCurrentTvshows | LoadAudiobookGenres;
