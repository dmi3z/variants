// import { Tvshow } from './../../app/models/tvshow';
import { STORE_ACTIONS, DataActions } from './data.action';
import { Channel } from 'src/app/models/channel';
import { Genre, Category } from 'src/app/models/category';
import { Tvshow } from 'src/app/models/vod';

export interface State {
  channels: Channel[];
  channelGenres: Genre[];
  vodCategories: Category[];
  currentTvshows: Tvshow[];
  audiobookGenres: Genre[];
}

const initialState: State = {
  channels: [],
  channelGenres: [],
  vodCategories: [],
  currentTvshows: [],
  audiobookGenres: []
};

export function DataReducer(state = initialState, action: DataActions) {
  switch (action.type) {
    case STORE_ACTIONS.LOAD_CHANNELS:
      return {
        ...state,
        channels: action.channels
      };
    case STORE_ACTIONS.LOAD_CHANNEL_GENRES:
      return {
        ...state,
        channelGenres: action.genres
      };

    case STORE_ACTIONS.LOAD_AUDIOBOOK_GENRES:
      return {
        ...state,
        audiobookGenres: action.genres
      };

    case STORE_ACTIONS.LOAD_VOD_CATEGORIES:
      return {
        ...state,
        vodCategories: action.categories
      };

    case STORE_ACTIONS.LOAD_CURRENT_TVSHOWS:
      return {
        ...state,
        currentTvshows: action.tvshows
      };

    default:
      return state;
  }
}
