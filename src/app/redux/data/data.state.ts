import { Channel } from '../../models/channel';
import { Genre, Category } from '../../models/category';
import { Tvshow } from 'src/app/models/vod';


export interface DataState {
  storeData: {
    channels: Channel[],
    channelGenres: Genre[],
    vodCategories: Category[],
    audiobookGenres: Genre[],
    currentTvshows: Tvshow[]
  };
}
